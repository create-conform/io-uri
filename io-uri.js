/////////////////////////////////////////////////////////////////////////////////////////////
//
// io-uri
//
//    IO URI Class.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Constants
//
/////////////////////////////////////////////////////////////////////////////////////////////
var ERROR_INVALID_URI =              "Invalid URI";
var ERROR_UNKNOWN_PROTOCOL =         "Unknown Protocol";
var ERROR_INVALID_PROTOCOL_HANDLER = "Invalid Protocol Handler";
var ERROR_NO_ENTRY =                 "No Entry";

var FORMAT_URI =      "uri";
var FORMAT_PATH =     "path";
var ENTRY_FILE =      "file";
var ENTRY_DIRECTORY = "directory";

var RE_URI = new RegExp(
    /^(([^:\/?#]+):)?(\/\/([^\/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?/
);
var RE_URI_AUTHORITY = new RegExp(
    /^(?!.*\n.*)(?:([^:]*)(?::(.*?))?@)?([^:]*)(?::([^:]*?))?$/
);

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var Error =     require("error");
var type  =     require("type");

var protocols = {};
var formats =   {};

/////////////////////////////////////////////////////////////////////////////////////////////
//
// URI Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function URI(scheme, authority, path, query, fragment, module) {
    var own = this;

    var parts;
    if (arguments.length <= 2 && type.isString(scheme) && type.isObject(authority)) {
        // rfc compliant - https://tools.ietf.org/html/rfc3986#appendix-B
        parts = RE_URI.exec(scheme);
        module = authority;
    }
    if (scheme instanceof URI) {
        authority = new URIAuthority(scheme.authority.userInfo, scheme.authority.host, scheme.authority.port);
        path = scheme.path;
        query = scheme.query;
        fragment = scheme.fragment;
        scheme = scheme.scheme;
    }

    this.scheme = parts ? parts[2] : scheme;
    this.authority = parts ? parts[4] : authority;
    this.path = parts ? parts[5] : path;
    this.query = parts ? parts[7] : query;
    this.fragment = parts ? parts[9] : fragment;

    var initialScheme = own.scheme;

    // check if path starts with slash
    if (this.path && this.path.length > 1 && this.path.substr(0, 1) != "/") {
        throw new Error(ERROR_INVALID_URI, "Path '" + this.path + "' does not start with '/'.");
    }

    // parse authority data
    if (!(this.authority instanceof URIAuthority)) {
        this.authority = new URIAuthority(this.authority);
    }

    var UNKNOWN_PROTOCOL_ERROR = "Unknown protocol '" + this.scheme + "://'.";

    function getModule() {
        // module is specified from the parse function. this is a fail-safe
        if (!module || own.scheme != initialScheme) {
            if (protocols[own.scheme]) {
                module = protocols[own.scheme];
                initialScheme = own.scheme;
            }
        }
        return module;
    }

    // returns promise with first parameter stream object when fulfilled
    this.open = function (opt_access, opt_create) {
        var module = getModule();
        if (!module) {
            throw new Error(ERROR_UNKNOWN_PROTOCOL, UNKNOWN_PROTOCOL_ERROR);
        }
        return new Promise(function (resolve, reject) {
            return module.open(own, opt_access, opt_create).then(function (stream) {
                resolve(stream);
            }).catch(reject);
        });
    };

    this.exists = function () {
        var module = getModule();
        if (!module) {
            throw new Error(ERROR_UNKNOWN_PROTOCOL, UNKNOWN_PROTOCOL_ERROR);
        }
        return new Promise(function (resolve, reject) {
            return module.exists(own).then(function (type) {
                resolve(type);
            }).catch(reject);
        });
    };

    this.delete = function () {
        var module = getModule();
        if (!module) {
            throw new Error(ERROR_UNKNOWN_PROTOCOL, UNKNOWN_PROTOCOL_ERROR);
        }
        return new Promise(function (resolve, reject) {
            return module.delete(own).then(function (type) {
                resolve(type);
            }).catch(reject);
        });
    };

    this.toString = function (opt_format, opt_param) {
        var module = getModule();
        // rfc compliant - https://tools.ietf.org/html/rfc3986#section-5.3
        if (!opt_format) {
            return (own.scheme ? own.scheme + ":" : "") +
                (own.authority ? "//" + own.authority : "") +
                (own.path ? own.path : "") +
                (own.query ? "?" + own.query : "") +
                (own.fragment ? "#" + own.fragment : "");
        }

        try {
            if (opt_format) {
                if (opt_format == FORMAT_PATH) {
                    if (type.isString(opt_param)) {
                        return own.path.replace(/\//g, opt_param);
                    } else {
                        return own.path;
                    }
                }
                if (!formats[opt_format]) {
                    return "";
                }
                return formats[opt_format].toString(own, opt_format);
            }
            else if (module) {
                return module.toString(own);
            }
        }
        catch (e) {
            //toString() must never fail
            return "";
        }
    };
};
URI.protocols = {};
URI.protocols.register = function (module, protocol, opt_formats) {
    if (!protocol) {
        throw new Error(ERROR_INVALID_PROTOCOL_HANDLER, "The 'protocol' parameter is missing.");
    }
    if (protocols[protocol]) {
        throw new Error(ERROR_INVALID_PROTOCOL_HANDLER, "Protocol '" + protocol + "://' is already registered.");
    }
    if (!module) {
        throw new Error(ERROR_INVALID_PROTOCOL_HANDLER, "The 'module' parameter is missing.");
    }
    if (opt_formats) {
        if (!type.isArray(opt_formats)) {
            throw new Error(ERROR_INVALID_PROTOCOL_HANDLER, "The 'formats' parameter should be an array of strings.");
        }
        else {
            //add formats to list
            for (var f in opt_formats) {
                formats[opt_formats[f]] = module;
            }
        }
    }

    //add protocol to list
    protocols[protocol] = module;
};

/////////////////////////////////////////////////////////////////////////////////////////////
//
// URIAuthority Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function URIAuthority(userInfo, host, port) {
    var parts;
    if (arguments.length == 1) {
        parts = RE_URI_AUTHORITY.exec(userInfo);
    }

    this.userInfo = parts ? (parts[1] ? parts[1] + (parts[2] ? ":" + parts[2] : "") : null) : userInfo;
    this.host = parts ? parts[3] : host;
    this.port = parts ? parts[4] : port;
};
URIAuthority.prototype.toString = function () {
    return (this.userInfo ? this.userInfo + "@" : "") + (this.host ? this.host : "") + (this.port ? ":" + this.port : "");
};

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = URI;
module.exports.ERROR_INVALID_URI = ERROR_INVALID_URI;
module.exports.ERROR_INVALID_PROTOCOL_HANDLER = ERROR_INVALID_PROTOCOL_HANDLER;
module.exports.ERROR_UNKNOWN_PROTOCOL = ERROR_UNKNOWN_PROTOCOL;
module.exports.ERROR_NO_ENTRY = ERROR_NO_ENTRY;
module.exports.FORMAT_URI = FORMAT_URI;
module.exports.FORMAT_PATH = FORMAT_PATH;
module.exports.ENTRY_FILE = ENTRY_FILE;
module.exports.ENTRY_DIRECTORY = ENTRY_DIRECTORY;